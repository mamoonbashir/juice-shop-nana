import request
headers = {
    'Authorization': 'Token 5801025df3c4210c8de5b2b1eeec83876eacaaa3'
}

url = 'https://demo.defectdojo.org/api/v2/import-scan/'

data = {
    'active': True,
    'verified': True,
    'scan_type': 'Gitleaks',
    'minimum_severity': 'low',
    'engagement': 36

}
files = {

    'file': open('gitleaks.json', 'rb')
}

response = request.post(url, data=data, headers=headers, files=files)

if response.status_code == 201:
    print ('Scan results imported successfully')
else:
    print (f'Scan result import failed{response.content}')
